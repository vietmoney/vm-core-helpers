const Joi = require('joi');
const headerBasic = {
    authorization: Joi.string()
      .regex(/^Basic /g)
      .required()
      .description('Format: Basic base64({clientSecret}.{clientId})')
  },
  headerBearer = {
    authorization: Joi.string()
      .regex(/^Bearer /g)
      .required()
      .description('Format: Bearer {token}')
  },
  headerMerge = {
    authorization: Joi.string()
      .regex(/(^Bearer |^Basic )/g)
      .required()
      .description(
        'Format: Basic base64({clientSecret}.{clientId}) or Bearer {token}'
      )
  };

module.exports = {
  headerBasic: headerBasic,
  headerBearer: headerBearer,
  headerMerge: headerMerge
};
