const path = require('path');
const multer = require('multer');
const Hoek = require('hoek');
const BPromise = require('bluebird');

class File {
  static upload(req, res, opts) {
    return new BPromise((resolve, reject) => {
      opts = opts || {};
      Hoek.assert(opts.destination, 'Empty destination in function uploadFile');

      const storage = multer.diskStorage({
          destination: path.join(
            opts.basePath || process.cwd(),
            opts.destination
          ),
          filename: (req, file, cb) => {
            return cb(
              null,
              `${opts.fileName}${path.extname(file.originalname)}`
            );
          }
        }),
        fileFilter = (req, file, cb) => {
          let listFilter = 'jpe?g|png|gif|bmp|svg';
          if (opts.fileFilter) {
            if (Array.isArray(opts.fileFilter) && opts.fileFilter.length > 0) {
              opts.fileFilter.forEach(e => {
                listFilter += `|${3}`;
              });
            } else {
              listFilter += `|${opts.fileFilter}`;
            }
          }
          const pattern = new RegExp(`.(${listFilter})$`, 'i');
          if (pattern.test(file.originalname)) cb(null, true);
          else {
            cb(
              {
                code: '107',
                source: 'Upload file'
              },
              false
            );
          }
        },
        upload = multer({
          storage: storage,
          fileFilter: fileFilter,
          limits: {
            fileSize: opts.fileSize || 2 * 1024 * 1024
          }
        }).single(opts.fieldFile);

      return upload(req, res, err => {
        if (err && err.code && err.code === 'LIMIT_FILE_SIZE')
          return reject({
            code: '516',
            source: 'Upload file',
            params: {
              fileSize: opts.fileSize / 1048576 || 2
            }
          });

        if (!req.file || err)
          return reject(
            err || {
              code: '106',
              source: 'Upload file'
            }
          );

        return resolve(
          Object.assign(req.file, {
            url: `${opts.destination}/${req.file.filename}`
          })
        );
      });
    });
  }
}

module.exports = File;
