class DateFormat {
  static getFirstLastDayForWeek() {
    const date = new Date(),
      monDayForDate = date.getDay(),
      monday = date.getDate() - monDayForDate + (monDayForDate === 0 ? -6 : 1),
      mondayDate = new Date(date.setDate(monday)),
      sundayDate = new Date(date.setTime(mondayDate.getTime() + 6 * 86400000));

    return {
      monday: `${mondayDate.getFullYear()}/${mondayDate.getMonth() + 1}/${mondayDate.getDate()}`,
      sunday: `${sundayDate.getFullYear()}/${sundayDate.getMonth() + 1}/${sundayDate.getDate()}`,
    };

  }

  static getFirstLastDayForThirty() {
    const lastDate = new Date(),
      firstDay = new Date(lastDate.getTime() - 30 * 60 * 60 * 24 * 1000);
    return {
      first: `${firstDay.getFullYear()}/${firstDay.getMonth() + 1}/${firstDay.getDate()}`,
      end: `${lastDate.getFullYear()}/${lastDate.getMonth() + 1}/${lastDate.getDate()}`,
    };
  }

  static getFirstLastDayForMonth() {
    const date = new Date(),
      firstDay = new Date(date.getFullYear(), date.getMonth(), 1),
      lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    return {
      first: `${firstDay.getFullYear()}/${firstDay.getMonth() + 1}/${firstDay.getDate()}`,
      end: `${lastDay.getFullYear()}/${lastDay.getMonth() + 1}/${lastDay.getDate()}`,
    };
  }

  static getDateString(format, date, opts) {
    opts = opts || {};
    date = date ? typeof date === 'object' ? date : new Date(isFinite(date) ? parseInt(date) : date) : new Date();
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      getPaddedComp = comp => {
        return ((parseInt(comp) < 10) ? (`0${comp}`) : comp);
      },
      o = {
        'y': date.getFullYear(), // year
        'M': months[date.getMonth()], //month
        'm': getPaddedComp(date.getMonth() + 1), //month
        'd': getPaddedComp(date.getDate()), //day
        'h': getPaddedComp((date.getHours() > 12) ? date.getHours() % 12 : date.getHours()), //hour
        'H': getPaddedComp(date.getHours()), //hour
        'i': getPaddedComp(date.getMinutes()), //minute
        's': getPaddedComp(date.getSeconds()), //second
        'S': getPaddedComp(date.getMilliseconds()), //millisecond,
        'b': (date.getHours() >= 12) ? 'PM' : 'AM'
      };

    for (let k in o) {
      if (new RegExp(`(${!opts.addText ? `${k}+` : `{${k}}+`})`).test(format)) {
        format = format.replace(RegExp.$1, o[k]);
      }
    }
    return format;
  }

}

module.exports = DateFormat;