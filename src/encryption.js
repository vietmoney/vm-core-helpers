/**
 * @Author: Tran Van Nhut (nhutdev)
 * @Date: 2017-08-22 15:09:16
 * @Email:  tranvannhut4495@gmail.com
 * @Last modified by:   nhutdev
 * @Last modified time: 2017-08-22 15:09:16
 */

const md5 = require('md5');
const secret = 'l)ff1c3L';
const itoa64 =
  './03bMhKQftsJHDqulZN2mYXkwcRxWU75yAnBCVdgEairL6vT81j9p4PoGSeFOIz';
const crypto = require('crypto');
const Hoek = require('hoek');

class Encryption {
  static hashPassword(pass) {
    pass = pass.toString();
    const encode = Encryption.encode64(pass, pass.length);
    let output = encode;

    for (let i = 0; i < encode.length; i++) {
      output += itoa64[encode[i].charCodeAt(0) % itoa64.length];
    }
    return itoa64[pass.length] + md5(secret + output);
  }

  static checkPassword(pass, hash) {
    const passHash = Encryption.hashPassword(pass);
    return passHash === hash;
  }

  static rc4(str, key) {
    key = key || secret;
    let s = [],
      j = 0,
      x,
      res = '';
    for (let i = 0; i < 256; i++) {
      s[i] = i;
    }
    for (let i = 0; i < 256; i++) {
      j = (j + s[i] + key.charCodeAt(i % key.length)) % 256;
      x = s[i];
      s[i] = s[j];
      s[j] = x;
    }
    let i = 0;
    j = 0;
    for (let y = 0; y < str.length; y++) {
      i = (i + 1) % 256;
      j = (j + s[i]) % 256;
      x = s[i];
      s[i] = s[j];
      s[j] = x;
      res += String.fromCharCode(str.charCodeAt(y) ^ s[(s[i] + s[j]) % 256]);
    }
    return res;
  }

  static encode64(input, count) {
    let output = '',
      i = 0,
      value = '';

    while (i < count) {
      value = input[i++].charCodeAt(0);
      output += itoa64[value & 0x3f];

      if (i < count) value |= input[i].charCodeAt(0) << 8;

      output += itoa64[(value >> 6) & 0x3f];
      if (i++ >= count) break;

      if (i < count) value |= input[i].charCodeAt(0) << 16;

      output += itoa64[(value >> 12) & 0x3f];

      if (i++ >= count) break;
      output += itoa64[(value >> 18) & 0x3f];
    }
    return output;
  }

  static encode(s, k) {
    let enc = '',
      str = '';
    k = k || secret;
    // make sure that input is string
    str = s.toString();
    for (let i = 0; i < s.length; i++) {
      // create block
      const a = s.charCodeAt(i);
      // bitwise XOR
      const b = a ^ k;
      enc = enc + String.fromCharCode(b);
    }
    return enc;
  }

  static cipher(text, key, opts = {}) {
    Hoek.assert(text, 'Data to create hash is not null');
    const algorithm = 'aes256' || opts.algorithm,
      cipher = crypto.createCipher(algorithm, key || secret),
      encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
    return encrypted;
  }

  static decipher(hash, key, opts = {}) {
    Hoek.assert(hash, 'Hash is not null');
    const algorithm = 'aes256' || opts.algorithm,
      decipher = crypto.createDecipher(algorithm, key || secret),
      decrypted = decipher.update(hash, 'hex', 'utf8') + decipher.final('utf8');

    return decrypted;
  }
}

module.exports = Encryption;
