const config = require('config');
const Hoek = require('hoek');
const DateHelper = require('./date');
const builder = require('xmlbuilder');

class Sitemap {

  static create(data, opts) {
    Hoek.assert(config.sitemap.xlsUrl && config.sitemap.url, '[Sitemap] Config fail , not exist "xlsUrl","url"');
    const configSitemap = config.sitemap;
    let obj = {
      '?xml-stylesheet': `type="text/xsl" href="${configSitemap.url}/${configSitemap.xlsUrl}"`
    };
    obj[opts.root] = {};
    obj[opts.root]['@xmlns'] = 'http://www.sitemaps.org/schemas/sitemap/0.9';
    obj[opts.root][opts.tree] = data;

    return builder.create(obj).end({
      pretty: true
    });
  }

  static index(data) {
    Hoek.assert(config.sitemap, '[Sitemap] config cannot null');
    let urlList = config.sitemap.sitemapUrls || [];
    urlList = urlList.map(e => {
      return {
        'loc': {
          '#text': `${config.sitemap.url}/${e}`
        },
        'lastmod': {
          '#text': (new Date()).toISOString()
        }
      };
    });
    data = Array.isArray(data) ? data : [data];
    data = data.map(e => {
      if (e.lastmod) {
        const lastmod = e.lastmod['#text'];
        e.lastmod['#text'] = typeof lastmod === 'object' ? lastmod.toISOString() : (new Date(lastmod)).toISOString();
      } else {
        e.lastmod = {};
        e.lastmod['#text'] = (new Date()).toISOString();
      }
      return e;
    });
    return Sitemap.create(urlList.concat(data), {
      root: 'sitemapindex',
      tree: 'sitemap'
    });
  }

  static urlset(data) {
    Hoek.assert(config.sitemap, '[Sitemap] config cannot null');
    data = Array.isArray(data) ? data : [data];
    data = data.map(e => {
      if (e.lastmod)
        e.lastmod = typeof e.lastmod === 'object' ? e.lastmod.toISOString() : (new Date(e.lastmod)).toISOString();
      else
        e.lastmod = (new Date()).toISOString();
      return e;
    });
    return Sitemap.create(data, {
      root: 'urlset',
      tree: 'url'
    });
  }

}

module.exports = Sitemap;